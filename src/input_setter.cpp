// input_setter.cpp



#include <controller/input_setter.h>



controller::Input::Input()
{
}


controller::Input::Input(size_t n_modes, size_t n_values) :
    modes(n_modes), values(n_values)
{
}



controller::InputSetter::InputSetter(unsigned int period) :
    cyclePeriod(boost::posix_time::milliseconds(period)),
    cycleStarted(false),
    id(0)
{
}


void controller::InputSetter::InternalThreadFunction(InputSetter* inputSetter)
{
    inputSetter->InternalCycleProcedure();
}


void controller::InputSetter::InternalCycleProcedure()
{
    Time start = boost::posix_time::microsec_clock::local_time();
    Time now;
    TimeDuration end;
    while (cycleStarted)
    {
        if (id == 0)
        {
            if ((id = GetNewControlID()) == 0)
            {
                // Wait 2 seconds before trying again.
                boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
                continue;
            }
            else
            {
                // We've got a new id.
                start = boost::posix_time::microsec_clock::local_time();
            }
        }

        now = boost::posix_time::microsec_clock::local_time();

        SendInput();
        start = start + cyclePeriod;

        end = start - now;
        if (!end.is_negative())
            boost::this_thread::sleep(end);
    }
}


void controller::InputSetter::StartCycle(bool start)
{
    mut.lock();
    if (!cycleStarted && start)
    {
        cycleStarted = true;
        internalThread = new boost::thread(InputSetter::InternalThreadFunction, this);
    }
    else if (cycleStarted && !start)
    {
        cycleStarted = false;
        internalThread->join();
    }
    mut.unlock();
}


unsigned char controller::InputSetter::CycleStarted()
{
    return cycleStarted;
}


unsigned char controller::InputSetter::GetID()
{
    return id;
}


void controller::InputSetter::SetInput(controller::Input & input, bool send)
{
    mut.lock();
    InputSetter::input = input;
    if (send && id != 0)
        SendInput();
    mut.unlock();
}


const controller::Input &controller::InputSetter::GetInput()
{
    return input;
}


controller::InputSetter::~InputSetter()
{
    StartCycle(false);
}
