// controller.cpp



#include <controller/controller.h>



controller::Controller::Controller()
{
    lastRequestedId = 1;

    stop = false;

    newInputReceived = false;

    lastInputWasAuto = false;

    cyclePeriod = boost::posix_time::milliseconds(1000);

    state = CONTROLLER_CONTROLLER_STATE_STOPPED;

    inputOriginChangePeriod = boost::posix_time::milliseconds(1000);
    timeOfLastInput = boost::posix_time::microsec_clock::local_time() - inputOriginChangePeriod;

    inputWithTimeout = false;
    timeoutForInput = boost::posix_time::milliseconds(2000);

    allowAuto = false;

    standardInputSet = false;
}


unsigned char controller::Controller::RequestID()
{
    lastRequestedId++;
    return lastRequestedId;
}


void controller::Controller::InternalThreadFunction(Controller* controller)
{
    controller->InternalCycleProcedure();
}


void controller::Controller::InternalCycleProcedure()
{
    Time now;
    TimeDuration end;
    while (!stop)
    {
        Time start = boost::posix_time::microsec_clock::local_time();

        InternalCycle();

        now = boost::posix_time::microsec_clock::local_time();
        //start = start + cyclePeriod;
        end =  cyclePeriod - (now - start);
        boost::this_thread::sleep(end);
    }
}


void controller::Controller::SetStandardInput()
{
}


void controller::Controller::PassNewInputToActualInput()
{
    mutNewtoActual.lock();

    Time localtime = boost::posix_time::microsec_clock::local_time();
    if (inputWithTimeout)
    {
        TimeDuration diff = localtime - timeOfLastInput;
        if (diff > timeoutForInput)
        {
            if (!standardInputSet)
            {
                std::cout << "input failed: timeout (no message for " << diff.total_milliseconds() <<  "ms); setting standard new input" << std::endl;
                standardInputSet = true;
                SetStandardInput();
            }
        }
        else
            standardInputSet = false;
    }
    originId = newOriginId;
    inputModes = inputNewModes;
    inputs = newInputs;
    mutNewtoActual.unlock();
}


void controller::Controller::Input(uint8_t originId, std::vector<unsigned char> modes, std::vector<double> inputs)
{
    bool inputOk = true;

    mutNewtoActual.lock();

    newInputReceived = true;

    if (originId == ID::Forbidden)
    {
        std::cout << "Input failed: forbidden ID" << std::endl;

        inputOk = false;
    }
    else if (originId == ID::Auto && !allowAuto)
    {
        std::cout << "Input failed: auto ID not allowed" << std::endl;

        inputOk = false;
    }
    else if (originId != ID::Auto || !allowAuto)
    {
        if (lastInputWasAuto && allowAuto)
        {
            std::cout << "Input failed: auto is running" << std::endl;

            inputOk = false;
        }
        else if (this->originId != originId)
        {
            if (boost::posix_time::microsec_clock::local_time() - timeOfLastInput < inputOriginChangePeriod && newOriginId != originId)
            {
                std::cout << "Input failed: another ID running " << std::endl;

                inputOk = false;
            }
        }
    }

    if (inputOk)
    {
        timeOfLastInput = boost::posix_time::microsec_clock::local_time();
        newOriginId = originId;
        inputNewModes = modes;
        newInputs = inputs;

        if (newOriginId == ID::Auto)
            lastInputWasAuto = true;
    }

    mutNewtoActual.unlock();
}


void controller::Controller::InternalCycle()
{
    if (newInputReceived && !allowAuto && !lastInputWasAuto)
        allowAuto = true;

    newInputReceived = false;
    lastInputWasAuto = false;
}


void controller::Controller::ExternalCycle()
{
    if (newInputReceived && !allowAuto && !lastInputWasAuto)
        allowAuto = true;

    newInputReceived = false;
    lastInputWasAuto = false;
}


void controller::Controller::StartInternalCycle()
{
    mutStartStop.lock();
    if (state == CONTROLLER_CONTROLLER_STATE_STOPPED)
    {
        state = CONTROLLER_CONTROLLER_STATE_STARTED;
        stop = false;
        internalThread = new boost::thread(Controller::InternalThreadFunction, this);
    }
    mutStartStop.unlock();
}


void controller::Controller::StopInternalCycle()
{
    mutStartStop.lock();
    if (state == CONTROLLER_CONTROLLER_STATE_STARTED)
    {
        state = CONTROLLER_CONTROLLER_STATE_STOPPED;
        stop = true;
        internalThread->join();
        delete internalThread;
        internalThread = nullptr;
    }
    mutStartStop.unlock();
}


bool controller::Controller::InternalCycleState()
{
    bool ret;
    mutStartStop.lock();
    ret = state;
    mutStartStop.unlock();
    return ret;
}


void controller::Controller::SetInternalCyclePeriod(TimeDuration period)
{
    cyclePeriod = period;
}


TimeDuration controller::Controller::GetInternalCyclePeriod()
{
    return cyclePeriod;
}


controller::Controller::~Controller()
{
    StopInternalCycle();
}
