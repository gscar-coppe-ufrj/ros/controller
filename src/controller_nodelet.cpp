// controller_nodelet.cpp



#include <controller/controller_nodelet.h>



controller::ControllerNodelet::ControllerNodelet()
{
}


void controller::ControllerNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();
    std::string cycleTopic;
    privateNH.param("cycle", cycleTopic, getName() + "/cycle");

    /*// maybe we should use arguments instead of ros parameters
    auto arg = getMyArgv();

    std::string search = "cycle=";
    std::string cycleTopic = getName() + "/Cycle";
    for (auto i = arg.begin(); i != arg.end(); i++)
    {
        auto p = (*i).find(search);
        if (p != std::string::npos)
        {
            cycleTopic = (*i).substr(search.size());
            break;
        }
    }*/


    nodeHandle = getNodeHandle();
    msgCycleSub = nodeHandle.subscribe(cycleTopic, 10, &ControllerNodelet::TreatCycle, this);

    // Setting size to 10 to avoid discarding messages from different controllers
    msgInputSub = nodeHandle.subscribe(getName() + "/input", 10, &ControllerNodelet::TreatInput, this);

    srvIdSer = nodeHandle.advertiseService(getName() + "/request_id", &ControllerNodelet::TreatRequestID, this);

    srvAllowAutoSer = nodeHandle.advertiseService(getName() + "/allow_auto", &ControllerNodelet::TreatAllowAuto, this);
    srvIgnoreAutoSer = nodeHandle.advertiseService(getName() + "/ignore_auto", &ControllerNodelet::TreatIgnoreAuto, this);

    srvStartInternalCycleSer = nodeHandle.advertiseService(getName() + "/start_internal_cycle", &ControllerNodelet::TreatStartInternalCycleCallback, this);
    srvStopInternalCycleSer = nodeHandle.advertiseService(getName() + "/stop_internal_cycle", &ControllerNodelet::TreatStopInternalCycleCallback, this);
}


void controller::ControllerNodelet::TreatCycle(const std_msgs::Empty::ConstPtr &msg)
{
    ExternalCycle();
}


void controller::ControllerNodelet::TreatInput(const controller::Control::ConstPtr & msg)
{
    Input(msg->originId, msg->modes, msg->data);
}


bool controller::ControllerNodelet::TreatRequestID(controller::RequestID::Request & req, controller::RequestID::Response & res)
{
    res.id = RequestID();
    return true;
}


bool controller::ControllerNodelet::TreatAllowAuto(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res)
{
    mutNewtoActual.lock();
    allowAuto = true;
    mutNewtoActual.unlock();
    return true;
}


bool controller::ControllerNodelet::TreatIgnoreAuto(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res)
{
    mutNewtoActual.lock();
    allowAuto = false;
    mutNewtoActual.unlock();
    return true;
}


bool controller::ControllerNodelet::TreatStartInternalCycleCallback(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res)
{
    StartInternalCycle();
    return true;
}


bool controller::ControllerNodelet::TreatStopInternalCycleCallback(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res)
{
    StopInternalCycle();
    return true;
}


controller::ControllerNodelet::~ControllerNodelet()
{
}
