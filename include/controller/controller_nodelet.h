/*!
 * \file controller_nodelet.h
 */



#ifndef CONTROLLER_CONTROLLER_NODELET_H
#define CONTROLLER_CONTROLLER_NODELET_H



#include <controller/controller.h>
#include <ros/ros.h>
#include <nodelet/nodelet.h>

#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>
#include <controller/Control.h>
#include <controller/RequestID.h>



namespace controller {



/*!
 * \class ControllerNodelet
 * \brief The ControllerNodelet class
 */
class ControllerNodelet : public Controller, public nodelet::Nodelet
{
protected:
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /// Variable not implemented.
    std::string controllerName;

    /// Subscriber to a channel that treats the cycle.
    ros::Subscriber msgCycleSub;
    /// Subscriber to a channel that treats the arrived messages.
    ros::Subscriber msgInputSub;

    /// Request ID service.
    ros::ServiceServer srvIdSer;
    /// Allow auto service.
    ros::ServiceServer srvAllowAutoSer;
    /// Ignore auto service.
    ros::ServiceServer srvIgnoreAutoSer;
    /// Start internal cycle service.
    ros::ServiceServer srvStartInternalCycleSer;
    /// Stop internal cycle service.
    ros::ServiceServer srvStopInternalCycleSer;

public:
    /*!
     * \brief ControllerNodelet does nothing.
     */
    ControllerNodelet();

    /*!
     * \brief onInit sets the ROS node, susbcribes and advertise services.
     */
    virtual void onInit();

    /*!
     * \brief TreatCycle calls the fucntion Controller::ExternalCycle().
     * \param msg Empty ROS message.
     */
    void TreatCycle(const std_msgs::Empty::ConstPtr & msg);
    /*!
     * \brief TreatInput calls the fucntion
     *      Controller::Input(msg->originId, msg->modes, msg->data).
     * \param msg ROS message that has arrived.
     */
    void TreatInput(const controller::Control::ConstPtr & msg);

    /*!
     * \brief TreatRequestID class the function Controller::RequestID().
     * \param req Service request.
     * \param res Service response.
     * \return Always true.
     */
    bool TreatRequestID(controller::RequestID::Request & req, controller::RequestID::Response & res);

    /*!
     * \brief TreatAllowAuto allows automatic messages.
     * \param req Empty request.
     * \param res Empty response.
     * \return Always true.
     */
    bool TreatAllowAuto(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res);
    /*!
     * \brief TreatIgnoreAuto ignores automatic messages.
     * \param req Empty request.
     * \param res Empty response.
     * \return Always true.
     */
    bool TreatIgnoreAuto(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res);

    /*!
     * \brief TreatStartInternalCycleCallback calls the function
     *      Controller::StartInternalCycleCallback.
     * \param req Empty request.
     * \param res Empty response.
     * \return Always true.
     */
    bool TreatStartInternalCycleCallback(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res);
    /*!
     * \brief TreatStopInternalCycleCallback calls the function
     *      Controller::StopInternalCycleCallback.
     * \param req Empty request.
     * \param res Empty response.
     * \return Always true.
     */
    bool TreatStopInternalCycleCallback(std_srvs::Empty::Request & req, std_srvs::Empty::Response & res);

    /*!
     * \brief ~ControllerNodelet does nothing.
     */
    virtual ~ControllerNodelet();
};



}



#endif
