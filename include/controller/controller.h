/*!
 * \file controller.h
 */



#ifndef CONTROLLER_CONTROLLER_H
#define CONTROLLER_CONTROLLER_H



#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



namespace controller {



#define CONTROLLER_CONTROLLER_STATE_STOPPED true
#define CONTROLLER_CONTROLLER_STATE_STARTED false



namespace IDs
{
  enum ID
  {
      Forbidden,
      Auto,
      FirstManual
  };
}
typedef IDs::ID ID;



/*!
 * \class Controller
 * \brief The Controller class controls the ROS messages received and
 *      sets automatic messages.
 */
class Controller
{
    bool standardInputSet;

    /// Last id used by the controller
    unsigned char lastRequestedId;

    /// Flag of the controller that indicates if the controller has started
    ///     or has stopped.
    bool state;
    /// Variable used to tell the controller to stop or not.
    bool stop;

    /// Variable used to tell if a new ROS message has arrived or not.
    bool newInputReceived;

    /// Variable used to tell if the last message received was an automatic
    ///     message or not.
    bool lastInputWasAuto;

    /// Period of time that takes to change the input.
    TimeDuration inputOriginChangePeriod;
    /// Time difference between the actual time and the
    ///     Controller::InputOriginChangePeriod.
    Time timeOfLastInput;

    /// Pointer used to control the thread responsible for the internal
    ///     cycle.
    boost::thread* internalThread;

    /*!
     * \brief InternalThreadFunction calls the function
     *      Controller::InternalCycleProcedure().
     * \param controller A pointer to the class Controller in order to
     *      access a protected function.
     */
    static void InternalThreadFunction(Controller* controller);
    /*!
     * \brief InternalCycleProcedure calls the function
     *      Controller:InternalCycle() from in each period of time
     *      specified.
     */
    virtual void InternalCycleProcedure();

protected:
    /// Period of time that takes to end one internal cycle.
    TimeDuration cyclePeriod;

    /// Mutex used to control the internal cycle thread.
    boost::mutex mutStartStop;
    /// Mutex used to control the external cycle thread.
    boost::mutex mutNewtoActual;

    /// Variable used to tell if the controller allows automatic messages
    ///     or not.
    bool allowAuto;

    /// Id from the last accepted message.
    unsigned char originId;
    /// Id from the ROS message that has arrived.
    unsigned char newOriginId;

    /// Modes from the last accepted message.
    std::vector<unsigned char> inputModes;
    /// Modes from the ROS message that has arrived.
    std::vector<unsigned char> inputNewModes;

    /// Inputs from the last accepted message.
    std::vector<double> inputs;
    /// Inputs from the ROS message that has arrived.
    std::vector<double> newInputs;

    /// Variable that has not been implemented yet.
    std::vector<unsigned char> outputModes;

    /// Variable that has not been implemented yet.
    std::vector<double> outputs;

    /// Indicates if the new message has a timeout or not.
    bool inputWithTimeout;
    /// Period of time that indicates the timeout of a message.
    TimeDuration timeoutForInput;

    /*!
     * \brief RequestID adds one to the last id used by the controller.
     * \return Returns the new id.
     */
    unsigned char RequestID();

    /*!
     * \brief SetStandardNewInput has not been implemented yet.
     */
    virtual void SetStandardInput();
    /*!
     * \brief PassNewInputToActualInput calls the function
     *      Controller::SetStandardNewInput() if the input has a timeout
     *      and sets the id, modes and inputs.
     */
    void PassNewInputToActualInput();

    /*!
     * \brief Input checks is the message received is an automatic
     *      message or not and treat it accordingly.
     * \param originId Id of the message received.
     * \param modes Modes of the message received.
     * \param inputs Inputs of the message received.
     */
    virtual void Input(uint8_t originId, std::vector<unsigned char> modes, std::vector<double> inputs);

    /*!
     * \brief InternalCycle checks if the last message was automatic,
     *      if the controller is allowing automatic messages and if a
     *      new message has arrived, than allows automatic messages for
     *      now on.
     */
    virtual void InternalCycle();
    /*!
     * \brief ExternalCycle checks if the last message was automatic,
     *      if the controller is allowing automatic messages and if a
     *      new message has arrived, than allows automatic messages for
     *      now on.
     */
    virtual void ExternalCycle();

public:
    /*!
     * \brief Controller sets the defaults values for the Controller's
     *      variables.
     */
    Controller();

    /*!
     * \brief StartInternalCycle starts the internal cycle of the
     *      controller.
     */
    void StartInternalCycle();
    /*!
     * \brief StopInternalCyle waits for the internal cycle to end then
     *      stops the controller.
     */
    void StopInternalCycle();
    /*!
     * \brief InternalCycleState gets the value of the variable
     *      Controller::state.
     * \return Returns the state of the controller.
     */
    bool InternalCycleState();
    /*!
     * \brief SetInternalCyclePeriod sets a new period of time for the
     *      internal cycle.
     * \param period New period of time.
     */
    void SetInternalCyclePeriod(TimeDuration period);
    /*!
     * \brief GetInternalCyclePeriod returns the period of time of the
     *      internal cycle.
     * \return Returns the protected period of time variable.
     */
    TimeDuration GetInternalCyclePeriod();

    /*!
     * \brief ~Controller calls the function
     *      Controller::StopInternalCycle().
     */
    virtual ~Controller();
};



}



#endif
