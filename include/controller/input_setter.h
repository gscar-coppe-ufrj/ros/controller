/*!
* \file input_setter.h
*/



#ifndef CONTROLLER_INPUT_SETTER_H
#define CONTROLLER_INPUT_SETTER_H


#include <functional>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <atomic>



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



namespace controller {



class Input
{
public:
    std::vector<unsigned char> modes;
    std::vector<double> values;

    Input();
    Input(size_t n_modes, size_t n_values);
};



class InputSetter
{
    Input input;

    boost::mutex mut;
    boost::thread* internalThread;
    std::atomic<bool> cycleStarted;

    unsigned char id;

    static void InternalThreadFunction(InputSetter* inpuSetter);
    virtual void InternalCycleProcedure();

protected:
    TimeDuration cyclePeriod;

public:
    InputSetter(unsigned int period = 1000);

    std::function<unsigned char()> GetNewControlID;
    std::function<void()> SendInput;
    void StartCycle(bool start);
    unsigned char CycleStarted();

    unsigned char GetID();

    void SetInput(Input & input, bool send = false);
    const Input & GetInput();

    virtual ~InputSetter();
};



}



#endif
