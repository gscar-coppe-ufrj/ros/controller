/*!
 * \file namespace.h
 */



#ifndef CONTROLLER_NAMESPACE_H
#define CONTROLLER_NAMESPACE_H



/*!
 * \namespace controller
 * \brief The controller namespace contains classes
 */
namespace controller {
}



#endif

